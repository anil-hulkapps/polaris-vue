<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ProductController extends Controller
{
    function getProducts(){
        $query = '{
              shop {
                name
              }
              products(first: 5) {
              pageInfo {
                      hasNextPage
                      hasPreviousPage
                    }
                edges {
                    cursor

                  node {
                    id
                    title
                    totalInventory
                    totalVariants
                    variants(first:2){
                        edges{
                          node{
                            title
                            sku
                            price
                          }
                        }
                      }
                  }
                }
              }
            }';

        $data = ['query'=>$query];
        $response = Http::withBasicAuth(env('SHOPIFY_API_KEY'),env('SHOPIFY_API_SECRET'))
            ->post('https://demo-anil.myshopify.com/admin/api/2021-04/graphql.json',$data);
        return $response->json()['data']['products'];
    }

    function showProduct($id) {
//        dd("gid://shopify/Product/$id");
        $productIdd = "gid://shopify/Product/".$id;
//        dd($productId);
        $query = ' query MyQuery ($productId: ID!)
                    { product(id: $productId) {
                        title
                        handle
                        totalInventory
                        totalVariants
                        variants(first:1){
                        edges{
                          node{
                            title
                            sku
                            price
                          }
                        }
                      }
                      }
                }';
        $data = ['query'=>$query,
                 "variables" => [
                         'productId' => $productIdd,
                 ]];
        $response = Http::withBasicAuth(env('SHOPIFY_API_KEY'),env('SHOPIFY_API_SECRET'))
            ->post('https://demo-anil.myshopify.com/admin/api/2021-04/graphql.json',$data);
        return $response->json();

    }

    function createProduct(){
        $query = 'mutation MyMutation($input: ProductInput!) {
                  productCreate(input: $input) {
                    product {
                    id
                    title
                     }
                    }
                  }
                   ';
        $data = [
            "query" => $query,
            "variables" => [
                "input" => [
                    'title' => 'New product 3',
                    'bodyHtml'=>'This is new product2'
                ]
            ]
        ];
        $response = Http::withBasicAuth(env('SHOPIFY_API_KEY'),env('SHOPIFY_API_SECRET'))
            ->post('https://demo-anil.myshopify.com/admin/api/2021-04/graphql.json',$data);
        return $response->json();
    }
    function updateProduct(){
        $query = '';
        $data = [
            "query" => $query,
            "variables" => [
                "input" => [
                    'id'=>'gid://shopify/Product/6695211204773',
                    'title' => 'Updated title',
                    'bodyHtml'=>'Updated body'
                ]
            ]
        ];
        $response = Http::withBasicAuth(env('SHOPIFY_API_KEY'),env('SHOPIFY_API_SECRET'))
            ->post('https://demo-anil.myshopify.com/admin/api/2021-04/graphql.json',$data);
        return $response->json();
    }
    function deleteProduct(){
        $query = 'mutation MyMutation ($input: ProductDeleteInput!){
                  productDelete(input: $input) {
                    shop {
                      id
                    }
                  }
                }
                ';
        $data = [
            "query" => $query,
            "variables" => [
                "input" => [
                    'id'=>'gid://shopify/Product/6706050498725',
                ]
            ]
        ];
        $response = Http::withBasicAuth(env('SHOPIFY_API_KEY'),env('SHOPIFY_API_SECRET'))
            ->post('https://demo-anil.myshopify.com/admin/api/2021-04/graphql.json',$data);
        return $response->json();
    }

    public function nextPage($cursor) {
        $query = 'query MyQuery($cursor : String){
              shop {
                name
              }
              products(first: 5, after: $cursor ) {
              pageInfo {
                      hasNextPage
                      hasPreviousPage
                    }
                edges {
                cursor
                  node {
                    id
                    title
                    totalInventory
                    totalVariants
                    variants(first:2){
                        edges{
                          node{
                            title
                            sku
                            price
                          }
                        }
                      }
                  }
                }
              }
            }';

        $data = ['query'=>$query,
                 "variables" => [
                     'cursor' => $cursor,
                 ]];
        $response = Http::withBasicAuth(env('SHOPIFY_API_KEY'),env('SHOPIFY_API_SECRET'))
            ->post('https://demo-anil.myshopify.com/admin/api/2021-04/graphql.json',$data);
        return $response->json()['data']['products'];
    }

    public function previousPage($cursor) {
        $query = 'query MyQuery($cursor : String){
              shop {
                name
              }
              products(last: 5, before: $cursor ) {
              pageInfo {
                      hasNextPage
                      hasPreviousPage
                    }
                edges {
                cursor
                  node {
                    id
                    title
                    totalInventory
                    totalVariants
                    variants(first:2){
                        edges{
                          node{
                            title
                            sku
                            price
                          }
                        }
                      }
                  }
                }
              }
            }';

        $data = ['query'=>$query,
                 "variables" => [
                     'cursor' => $cursor,
                 ]];
        $response = Http::withBasicAuth(env('SHOPIFY_API_KEY'),env('SHOPIFY_API_SECRET'))
            ->post('https://demo-anil.myshopify.com/admin/api/2021-04/graphql.json',$data);
        return $response->json()['data']['products'];
    }

}
