
import products from "./vue/components/products";
import show from "./vue/components/show";

export default [
    { path: '/', name:'products', component: products},
    { path: '/show', name:'show', component: show},
]
