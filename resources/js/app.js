require('./bootstrap');

import Vue from 'vue';
import App from './vue/components/App';
import VueRouter from 'vue-router'
import routes from "./routes";
import PolarisVue from '@hulkapps/polaris-vue';
import '@hulkapps/polaris-vue/dist/polaris-vue.css';

Vue.use(VueRouter);
Vue.use(PolarisVue);

const router = new VueRouter({
    routes: routes,
    mode: 'history',
    linkActiveClass: 'active',
});

let app = new Vue({
    el : '#app',
    components:{
        App
    },
    router:router,
    render:h=>h(App)
})
